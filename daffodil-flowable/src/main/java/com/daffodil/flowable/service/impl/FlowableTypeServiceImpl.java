package com.daffodil.flowable.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.constant.Constants;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.core.entity.Ztree;
import com.daffodil.core.exception.BaseException;
import com.daffodil.flowable.entity.ActCnModelType;
import com.daffodil.flowable.service.IFlowableTypeService;
import com.daffodil.framework.shiro.util.ShiroUtils;
import com.daffodil.util.HqlUtils;
import com.daffodil.util.StringUtils;

/**
 * 
 * @author yweijian
 * @date 2020年2月11日
 * @version 1.0
 */
@Service
public class FlowableTypeServiceImpl implements IFlowableTypeService {

	@Autowired
	private JpaDao jpaDao;
	
	@Override
	public List<ActCnModelType> selectModelTypeList(Query<ActCnModelType> query) {
		StringBuffer hql  = new StringBuffer("from ActCnModelType where 1=1 ");
		List<Object> paras = new ArrayList<Object>();
		HqlUtils.createHql(hql, paras, query);
		return jpaDao.search(hql.toString(), paras, ActCnModelType.class, query.getPage());
	}

	@Override
	public List<Ztree> selectModelTypeTree(Query<ActCnModelType> query) {
		List<ActCnModelType> typeList = this.selectModelTypeList(query);
		List<Ztree> ztrees = this.initZtree(typeList);
		return ztrees;
	}

	@Override
	@Transactional
	public void deleteModelTypeById(String typeId) {
		int count = jpaDao.count("from ActCnModelType where parentId = ?", typeId);
		if(count > 0){
			throw new BaseException("删除流程类型失败，存在下级流程类型，不允许删除");
		}
		/*int count2 = jpaDao.count("from SysUser where deptId = ?", typeId);
		if(count2 > 0){
			throw new BaseException("删除部门失败，部门已分配，不允许删除");
		}*/
		jpaDao.delete(ActCnModelType.class, typeId);
	}

	@Override
	@Transactional
	public void insertModelType(ActCnModelType modelType) {
		if (this.checkModelTypeNameUnique(modelType)) {
			throw new BaseException("新增流程类型【" + modelType.getTypeName() + "】失败，流程类型名称已存在");
		}
		ActCnModelType parentType = this.selectModelTypeById(modelType.getParentId());
		if(StringUtils.isNotEmpty(parentType.getAncestors())){
			modelType.setAncestors(parentType.getAncestors() + "," + modelType.getParentId());
		}else{
			modelType.setAncestors(modelType.getParentId());
		}
		modelType.setCreateBy(ShiroUtils.getLoginName());
		modelType.setCreateTime(new Date());
		jpaDao.save(modelType);
	}

	@Override
	@Transactional
	public void updateModelType(ActCnModelType modelType) {
		if (this.checkModelTypeNameUnique(modelType)) {
			throw new BaseException("修改流程类型【" + modelType.getTypeName() + "】失败，流程类型名称已存在");
		}
		if (checkModelTypeIsSelfOrChildren(modelType)) {
			throw new BaseException("修改流程类型【" + modelType.getTypeName() + "】失败，上级流程类型不能是自己或自己的子流程类型");
		}
		ActCnModelType actCnModelType = this.selectModelTypeById(modelType.getId());
		ActCnModelType parent= this.selectModelTypeById(modelType.getParentId());
		if (StringUtils.isNotNull(parent) && StringUtils.isNotNull(actCnModelType)) {
			String newAncestors = parent.getAncestors() + "," + parent.getId();
			String oldAncestors = actCnModelType.getAncestors();
			modelType.setAncestors(newAncestors);
			this.updateChildrenModelType(modelType, newAncestors, oldAncestors);
		}
		modelType.setUpdateBy(ShiroUtils.getLoginName());
		modelType.setUpdateTime(new Date());
		jpaDao.update(modelType);
	}
	
	@Override
	public ActCnModelType selectModelTypeById(String typeId) {
		if(Constants.ROOT.equals(typeId)){
			ActCnModelType modelType = new ActCnModelType();
			modelType.setId(Constants.ROOT);
			modelType.setTypeName("流程类型");
			modelType.setAncestors("");
			return modelType;
		}
		return jpaDao.find(ActCnModelType.class, typeId);
	}

	@Override
	public boolean checkModelTypeNameUnique(ActCnModelType modelType) {
		List<Object> paras = new ArrayList<Object>();
		String hql = "from ActCnModelType where typeName=? and parentId=? ";
		paras.add(modelType.getTypeName());
		paras.add(modelType.getParentId());
		if(StringUtils.isNotEmpty(modelType.getId())){
			hql += "and id != ?";
			paras.add(modelType.getId());
		}
		ActCnModelType actCnModelType = jpaDao.find(hql, paras, ActCnModelType.class);
		if (StringUtils.isNotNull(actCnModelType)) {
			return Constants.NOT_UNIQUE;
		}
		return Constants.IS_UNIQUE;
	}
	
	/**
	 * 初始化zTree
	 * @param types 流程类型列表
	 * @return 树结构列表
	 */
	public List<Ztree> initZtree(List<ActCnModelType> types) {
		List<Ztree> ztrees = new ArrayList<Ztree>();
		for (ActCnModelType modelType : types) {
			if (Constants.NORMAL.equals(modelType.getStatus())) {
				Ztree ztree = new Ztree();
				ztree.setId(modelType.getId());
				ztree.setpId(modelType.getParentId());
				ztree.setName(modelType.getTypeName());
				ztree.setTitle(modelType.getTypeName());
				ztrees.add(ztree);
			}
		}
		return ztrees;
	}
	
	/**
	 * 检查是否是自己或者自己的子流程类型
	 * @param modelType
	 * @return
	 */
	public boolean checkModelTypeIsSelfOrChildren(ActCnModelType modelType){
		List<Object> paras = new ArrayList<Object>();
		String hql = "from ActCnModelType where id = ? or ancestors like ?";
		paras.add(modelType.getId());
		paras.add("%" + modelType.getId() + "%");
		List<ActCnModelType> types = jpaDao.search(hql, paras, ActCnModelType.class);
		for(ActCnModelType actCnModelType : types){
			if(modelType.getParentId().equals(actCnModelType.getId())){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 更新子流程类型
	 * @param dept
	 * @param newAncestors
	 * @param oldAncestors
	 */
	@Transactional
	public void updateChildrenModelType(ActCnModelType modelType, String newAncestors, String oldAncestors) {
		//查询旧的前缀的所有子流程类型
		List<ActCnModelType> childrens = jpaDao.search("from ActCnModelType where ancestors like ?", "%" + modelType.getId() + "%", ActCnModelType.class);
		for (ActCnModelType child : childrens) {
			//替换掉所有子流程类型的前缀
			child.setAncestors(child.getAncestors().replace(oldAncestors, newAncestors));
			//父组织状态改变，子流程类型也变更
			child.setStatus(modelType.getStatus());
			jpaDao.update(child);
		}
	}

}
