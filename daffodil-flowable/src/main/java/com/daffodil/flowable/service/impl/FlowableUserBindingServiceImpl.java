package com.daffodil.flowable.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.flowable.engine.ProcessEngine;
import org.flowable.engine.repository.Deployment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.constant.Constants;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.exception.BaseException;
import com.daffodil.flowable.entity.ActCnModelUser;
import com.daffodil.flowable.service.IFlowableUserBindingService;

/**
 * 
 * @author yweijian
 * @date 2020年2月11日
 * @version 1.0
 */
@Service
public class FlowableUserBindingServiceImpl implements IFlowableUserBindingService {

	@Autowired
	private JpaDao jpaDao;
	
	@Autowired
	private ProcessEngine processEngine;
	
	@Override
	@Transactional
	public void bingdingByDeployId(String deployId, String userId) {
		
		Deployment deployment = processEngine.getRepositoryService().createDeploymentQuery().deploymentId(deployId).singleResult();
		if(null == deployment){
			throw new BaseException("流程绑定失败，流程实例不存在");
		}
		String hql = "from ActCnModelUser where status = ? and typeId = ? and userId = ?";
		List<Object> paras = new ArrayList<Object>();
		paras.add(Constants.NORMAL);
		paras.add(deployment.getTenantId());
		paras.add(userId);
		ActCnModelUser checkModelUser = jpaDao.find(hql, paras, ActCnModelUser.class);
		//同类型只能绑定一个
		if(null != checkModelUser && !checkModelUser.getDeployId().equals(deployId)){
			checkModelUser.setStatus(Constants.DELETED);
			jpaDao.update(checkModelUser);
		}else if(null != checkModelUser && checkModelUser.getDeployId().equals(deployId)){
			return;
		}
		
		ActCnModelUser modelUser = new ActCnModelUser();
		modelUser.setDeployId(deployment.getId());
		modelUser.setTypeId(deployment.getTenantId());
		modelUser.setModelKey(deployment.getKey());
		modelUser.setUserId(userId);
		modelUser.setStatus(Constants.NORMAL);
		jpaDao.save(modelUser);
	}

}
