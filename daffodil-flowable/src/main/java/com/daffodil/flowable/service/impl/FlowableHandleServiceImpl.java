package com.daffodil.flowable.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import org.flowable.engine.ProcessEngine;
import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.history.HistoricProcessInstanceQuery;
import org.flowable.engine.task.Comment;
import org.flowable.task.api.Task;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.flowable.task.api.history.HistoricTaskInstanceQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.Query;
import com.daffodil.flowable.entity.ActCnHistoricActivity;
import com.daffodil.flowable.entity.ActCnHistoricProcess;
import com.daffodil.flowable.service.IFlowableHandleService;
import com.daffodil.flowable.util.FlowableUtils;
import com.daffodil.framework.shiro.util.ShiroUtils;
import com.daffodil.util.StringUtils;

/**
 * 通用流程办理实现
 * 
 * @author yweijian
 * @date 2020年1月16日
 * @version 1.0
 */
@Service
public class FlowableHandleServiceImpl implements IFlowableHandleService {

	private static final Logger LOGGER = LoggerFactory.getLogger(FlowableHandleServiceImpl.class);

	@Autowired
	private ProcessEngine processEngine;

	@Override
	@Transactional
	public void generalDirectHandle(String businessKey, String handleId, String message, String userId,
			String attachmentName, String attachmentDescription, MultipartFile attachmentFile) {
		// 获取当前任务
		Task task = FlowableUtils.getCurrentTask(businessKey);
		// 保存意见表
		processEngine.getIdentityService().setAuthenticatedUserId(ShiroUtils.getLoginName());
		processEngine.getTaskService().addComment(task.getId(), task.getProcessInstanceId(), message);

		// 保存附件表-后面实现
		if (null != attachmentFile) {
			try {
				processEngine.getTaskService().createAttachment(attachmentFile.getContentType(), task.getId(),
						task.getProcessInstanceId(), attachmentName, attachmentDescription,
						attachmentFile.getInputStream());
			} catch (IOException e) {
				LOGGER.warn("流程办理附件保存失败");
			}
		}
		
		Map<String, Object> handleVariables = FlowableUtils.getHandleVariables(businessKey, handleId);
		Map<String, Object> processVariables = task.getProcessVariables();
		Map<String, Object> variables = new HashMap<String, Object>();
		if(StringUtils.isNotNull(handleVariables)){
			variables.putAll(handleVariables);
		}
		if(StringUtils.isNotNull(processVariables)){
			variables.putAll(processVariables);
		}
		// 保存结束任务，并会新开启下一个任务
		processEngine.getTaskService().complete(task.getId(), variables);

		// 开启的新任务（下一个任务）
		Task currentTask = FlowableUtils.getCurrentTask(businessKey);
		
		// 设置下一步任务操作人员,否则任务不存在即该流程任务结束了
		if (currentTask != null) {
			processEngine.getTaskService().setAssignee(currentTask.getId(), userId);
		}

	}

	@Override
	public List<ActCnHistoricProcess> selectMyselfProcessList(Query<ActCnHistoricProcess> query, String type) {
		if ("0".equals(type)) {// 待提交-环节任务待办-流程未结
			return this.selectMyselfProcessList(query, false, false);
		} else if ("1".equals(type)) {// 已提交-环节任务已办-流程未结
			return this.selectMyselfProcessList(query, true, false);
		} else if ("2".equals(type)) {// 已办结-环节任务已办-流程结束
			return this.selectMyselfProcessList(query, true, true);
		} else {
			return null;
		}
	}

	@Override
	public List<ActCnHistoricProcess> selectUnfinishedProcessList(Query<ActCnHistoricProcess> query) {
		return this.selectHistoricProcessList(query, false);
	}

	@Override
	public List<ActCnHistoricProcess> selectFinishedProcessList(Query<ActCnHistoricProcess> query) {
		return this.selectHistoricProcessList(query, true);
	}

	@Override
	public List<ActCnHistoricProcess> selectCompleteProcessList(Query<ActCnHistoricProcess> query) {
		List<ActCnHistoricProcess> datas = null;
		List<HistoricProcessInstance> list = null;
		HistoricProcessInstanceQuery processQuery = processEngine.getHistoryService()
				.createHistoricProcessInstanceQuery();
		if (StringUtils.isNotEmpty(query.getEntity().getName())) {
			processQuery.processInstanceNameLike("%" + query.getEntity().getName() + "%");
		}
		if (StringUtils.isNotNull(query.getPage())) {
			Page page = query.getPage();
			page.setTotalRow((int) processQuery.count());
			list = processQuery.finished().orderByProcessInstanceStartTime().desc().listPage(page.getFromIndex(),
					page.getPageSize());
		} else {
			list = processQuery.finished().orderByProcessInstanceStartTime().desc().list();
		}
		if (StringUtils.isNotEmpty(list)) {
			datas = new ArrayList<ActCnHistoricProcess>();
			for (int i = 0; i < list.size(); i++) {
				HistoricProcessInstance hpi = list.get(i);
				ActCnHistoricProcess hp = new ActCnHistoricProcess(hpi);
				datas.add(hp);
			}
		}
		return datas;
	}

	/**
	 * 查询个人环节任务待办且流程未结束的流程列表
	 * 
	 * @param query
	 * @return
	 */
	private List<ActCnHistoricProcess> selectMyselfProcessList(Query<ActCnHistoricProcess> query,
			boolean taskIsFinished, boolean processIsfinished) {

		List<HistoricTaskInstance> tasks = null;
		List<ActCnHistoricProcess> datas = null;
		List<HistoricProcessInstance> list = null;
		Set<String> processInstanceIds = new HashSet<String>();
		HistoricTaskInstanceQuery taskQuery = processEngine.getHistoryService().createHistoricTaskInstanceQuery();
		HistoricProcessInstanceQuery processQuery = processEngine.getHistoryService()
				.createHistoricProcessInstanceQuery().startedBy(ShiroUtils.getLoginName());

		if (!taskIsFinished && !processIsfinished) {// 环节待办，流程未结束
			// tasks = taskQuery.unfinished().processUnfinished().list();
			tasks = taskQuery.taskAssignee(ShiroUtils.getLoginName()).unfinished().processUnfinished().list();
		} else if (taskIsFinished && !processIsfinished) {// 环节已办，流程未结束
			tasks = taskQuery.finished().processUnfinished().list();
		} else if (taskIsFinished && processIsfinished) {// 环节已办，流程结束
			tasks = taskQuery.finished().processFinished().list();
		}

		if (StringUtils.isNotEmpty(tasks)) {
			for (int i = 0; i < tasks.size(); i++) {
				HistoricTaskInstance task = tasks.get(i);
				processInstanceIds.add(task.getProcessInstanceId());
			}
		}

		// 从指定的未完成任务且流程未结束的流程实例ID中查询，如果没有则结束查询
		if (StringUtils.isNotEmpty(processInstanceIds)) {
			processQuery.processInstanceIds(processInstanceIds);
		} else {
			return datas;
		}
		if (StringUtils.isNotEmpty(query.getEntity().getName())) {
			processQuery.processInstanceNameLike("%" + query.getEntity().getName() + "%");
		}
		if (StringUtils.isNotNull(query.getPage())) {
			Page page = query.getPage();
			page.setTotalRow((int) processQuery.count());
			list = processQuery.orderByProcessInstanceStartTime().desc().listPage(page.getFromIndex(),
					page.getPageSize());
		} else {
			list = processQuery.orderByProcessInstanceStartTime().desc().list();
		}
		if (StringUtils.isNotEmpty(list)) {
			datas = new ArrayList<ActCnHistoricProcess>();
			for (int i = 0; i < list.size(); i++) {
				HistoricProcessInstance hpi = list.get(i);
				ActCnHistoricProcess hp = new ActCnHistoricProcess(hpi);
				datas.add(hp);
			}
		}
		return datas;
	}

	/**
	 * 
	 * @param query
	 * @param taskIsFinished
	 * @return
	 */
	private List<ActCnHistoricProcess> selectHistoricProcessList(Query<ActCnHistoricProcess> query,
			boolean taskIsFinished) {
		List<HistoricTaskInstance> tasks = null;
		List<ActCnHistoricProcess> datas = null;
		List<HistoricProcessInstance> list = null;
		Set<String> processInstanceIds = new HashSet<String>();
		HistoricTaskInstanceQuery taskQuery = processEngine.getHistoryService().createHistoricTaskInstanceQuery();
		HistoricProcessInstanceQuery processQuery = processEngine.getHistoryService()
				.createHistoricProcessInstanceQuery();

		if (!taskIsFinished && !ShiroUtils.isAdmin()) {// 环节待办
			tasks = taskQuery.taskAssignee(ShiroUtils.getLoginName()).unfinished().processUnfinished().list();
		} else if (taskIsFinished && !ShiroUtils.isAdmin()) {// 环节已办
			tasks = taskQuery.taskAssignee(ShiroUtils.getLoginName()).finished().list();
		} else if (!taskIsFinished && ShiroUtils.isAdmin()) {// 环节待办，管理员
			tasks = taskQuery.unfinished().processUnfinished().list();
		} else if (taskIsFinished && ShiroUtils.isAdmin()) {// 环节已办，管理员
			tasks = taskQuery.finished().list();
		}

		if (StringUtils.isNotEmpty(tasks)) {
			for (int i = 0; i < tasks.size(); i++) {
				HistoricTaskInstance task = tasks.get(i);
				// 排除自己发起，自己提交的任务流程
				HistoricProcessInstance instance = processEngine.getHistoryService()
						.createHistoricProcessInstanceQuery().processInstanceId(task.getProcessInstanceId())
						.singleResult();
				if (null != instance && instance.getStartUserId().equals(ShiroUtils.getLoginName())) {
					continue;
				}
				processInstanceIds.add(task.getProcessInstanceId());
			}
		}
		// 从指定的未完成任务且流程未结束的流程实例ID中查询，如果没有则结束查询
		if (StringUtils.isNotEmpty(processInstanceIds)) {
			processQuery.processInstanceIds(processInstanceIds);
		} else {
			return datas;
		}
		if (StringUtils.isNotEmpty(query.getEntity().getName())) {
			processQuery.processInstanceNameLike("%" + query.getEntity().getName() + "%");
		}
		if (StringUtils.isNotNull(query.getPage())) {
			Page page = query.getPage();
			page.setTotalRow((int) processQuery.count());
			list = processQuery.orderByProcessInstanceStartTime().desc().listPage(page.getFromIndex(),
					page.getPageSize());
		} else {
			list = processQuery.orderByProcessInstanceStartTime().desc().list();
		}
		if (StringUtils.isNotEmpty(list)) {
			datas = new ArrayList<ActCnHistoricProcess>();
			for (int i = 0; i < list.size(); i++) {
				HistoricProcessInstance hpi = list.get(i);
				ActCnHistoricProcess hp = new ActCnHistoricProcess(hpi);
				datas.add(hp);
			}
		}
		
		return datas;
	}

	@Override
	public List<ActCnHistoricActivity> selectHistoricActivityByBusinessKey(String businessKey) {
		List<ActCnHistoricActivity> datas = null;
		HistoricProcessInstance instance = processEngine.getHistoryService().createHistoricProcessInstanceQuery()
				.processInstanceBusinessKey(businessKey).singleResult();
		if (null == instance) {
			return datas;
		}
		List<HistoricActivityInstance> list = processEngine.getHistoryService().createHistoricActivityInstanceQuery()
				.processInstanceId(instance.getId()).orderByHistoricActivityInstanceStartTime().asc().list();
		if (StringUtils.isNotEmpty(list)) {
			datas = new ArrayList<ActCnHistoricActivity>();
			for (int i = 0; i < list.size(); i++) {
				HistoricActivityInstance hai = list.get(i);
				ListIterator<Comment> it = processEngine.getTaskService().getTaskComments(hai.getTaskId())
						.listIterator();
				ActCnHistoricActivity ha = new ActCnHistoricActivity(hai, it);
				datas.add(ha);
			}
		}
		return datas;
	}

}