package com.daffodil.flowable.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.daffodil.core.entity.Query;
import com.daffodil.flowable.entity.ActCnHistoricActivity;
import com.daffodil.flowable.entity.ActCnHistoricProcess;

/**
 * 通用流程办理实现接口
 * @author yweijian
 * @date 2020年1月16日
 * @version 1.0
 */
public interface IFlowableHandleService {

	/**
	 * 直接办理 <br>
	 * 一般性的流程办理，在流程过程的输出线中无特殊的、复杂的、逻辑运算的排他网关 <br>
	 * 基本上能够满足绝大部分的一般性流程办理
	 * @param businessKey 业务ID
	 * @param handleId 操作ID
	 * @param message 办理意见
	 * @param userId 下一步操作人员（登录账号）
	 * @param attachmentName 附件名称
	 * @param attachmentDescription 附件备注描述
	 * @param attachmentFile 附件流
	 */
	public void generalDirectHandle(String businessKey, String handleId, String message, String userId,
			String attachmentName, String attachmentDescription, MultipartFile attachmentFile);
	
	/**
	 * 我的任务 <br>
	 * 根据查询条件查询本人发起的流程任务 <br>
	 * @param query
	 * @return
	 */
	public List<ActCnHistoricProcess> selectMyselfProcessList(Query<ActCnHistoricProcess> query, String type);
	
	/**
	 * 待办任务<br>
	 * 根据查询条件查询他人提交给本人的待办任务
	 * @param query
	 * @return
	 */
	public List<ActCnHistoricProcess> selectUnfinishedProcessList(Query<ActCnHistoricProcess> query);
	
	/**
	 * 已办任务<br>
	 * 根据查询条件查询他人提交给本人的已办任务
	 * @param query
	 * @return
	 */
	public List<ActCnHistoricProcess> selectFinishedProcessList(Query<ActCnHistoricProcess> query);
	
	/**
	 * 办结任务<br>
	 * 根据查询条件查询他人提交给本人的办结任务（主要提供给管理员使用）
	 * @param query
	 * @return
	 */
	public List<ActCnHistoricProcess> selectCompleteProcessList(Query<ActCnHistoricProcess> query);
	
	/**
	 * 根据业务ID查询流程步骤详情
	 * @param businessKey
	 * @return
	 */
	public List<ActCnHistoricActivity> selectHistoricActivityByBusinessKey(String businessKey);
	
}
