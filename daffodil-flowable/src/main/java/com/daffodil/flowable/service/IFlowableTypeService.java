package com.daffodil.flowable.service;

import java.util.List;

import com.daffodil.core.entity.Query;
import com.daffodil.core.entity.Ztree;
import com.daffodil.flowable.entity.ActCnModelType;

/**
 * 流程类型接口
 * @author yweijian
 * @date 2020年2月11日
 * @version 1.0
 */
public interface IFlowableTypeService {
	/**
	 * 根据查询条件查询流程类型
	 * @param query
	 * @return
	 */
	public List<ActCnModelType> selectModelTypeList(Query<ActCnModelType> query);

	/**
	 * 按树结构查询流程类型
	 * @param query
	 * @return
	 */
	public List<Ztree> selectModelTypeTree(Query<ActCnModelType> query);

	/**
	 * 删除流程类型管理信息
	 * @param typeId
	 */
	public void deleteModelTypeById(String typeId);

	/**
	 * 新增保存流程类型信息
	 * @param modelType
	 */
	public void insertModelType(ActCnModelType modelType);

	/**
	 * 修改保存流程类型信息
	 * @param modelType
	 */
	public void updateModelType(ActCnModelType modelType);

	/**
	 * 根据流程类型ID查询信息
	 * @param typeId
	 * @return
	 */
	public ActCnModelType selectModelTypeById(String typeId);

	/**
	 * 校验流程类型名称是否唯一
	 * @param modelType
	 * @return
	 */
	public boolean checkModelTypeNameUnique(ActCnModelType modelType);
}
