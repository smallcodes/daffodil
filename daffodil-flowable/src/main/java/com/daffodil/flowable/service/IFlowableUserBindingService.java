package com.daffodil.flowable.service;

/**
 * 流程用户绑定接口
 * @author yweijian
 * @date 2020年2月11日
 * @version 1.0
 */
public interface IFlowableUserBindingService {

	/**
	 * 用户绑定流程
	 * @param deployId
	 * @param userId
	 */
	public void bingdingByDeployId(String deployId, String userId);
}
