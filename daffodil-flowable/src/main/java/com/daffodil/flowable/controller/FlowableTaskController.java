package com.daffodil.flowable.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.flowable.bpmn.model.FlowElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.daffodil.core.annotation.Log;
import com.daffodil.core.annotation.Log.BusinessType;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableInfo;
import com.daffodil.flowable.entity.ActCnHistoricActivity;
import com.daffodil.flowable.entity.ActCnHistoricProcess;
import com.daffodil.flowable.service.IFlowableDiagramService;
import com.daffodil.flowable.service.IFlowableHandleService;
import com.daffodil.flowable.util.FlowableUtils;
import com.daffodil.system.controller.SystemController;
import com.daffodil.system.entity.SysUser;
import com.daffodil.util.file.FileUtils;

/**
 * 流程任务管理公共组件
 * @author yweijian
 * @date 2020年1月14日
 * @version 1.0
 */
@Controller
@RequestMapping("/flowable/task")
public class FlowableTaskController extends SystemController {

	private String prefix = "/flowable/task";
	
	@Autowired
	private IFlowableHandleService handleService;
	
	@Autowired
	private IFlowableDiagramService diagramService;
	
	/**
	 * 我的任务<br>
	 * @return
	 */
	@RequiresPermissions("flowable:task:view")
	@GetMapping("/myself/{type}")
	public String myself(@PathVariable("type") String type, ModelMap modelMap){
		modelMap.put("type", type);
		return prefix + "/myself";
	}
	
	/**
	 * 任务数据
	 * @param historicProcess
	 * @param type 0=待提交 1=已办结
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequiresPermissions("flowable:task:list")
	@PostMapping("/myselfList")
	@ResponseBody
	public TableInfo myselfList(ActCnHistoricProcess historicProcess, String type){
		initQuery(historicProcess, new Page());
		List<ActCnHistoricProcess> list = handleService.selectMyselfProcessList(query, type);
		return initTableInfo(list, query);
	}
	
	/**
	 * 所有待办任务，管理员可以看所有，否则只能看自己拥有的
	 * @return
	 */
	@RequiresPermissions("flowable:task:view")
	@GetMapping("/unfinished")
	public String unfinished(){
		return prefix + "/unfinished";
	}
	
	/**
	 * 任务数据
	 * @param historicProcess
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequiresPermissions("flowable:task:list")
	@PostMapping("/unfinishedList")
	@ResponseBody
	public TableInfo unfinishedList(ActCnHistoricProcess historicProcess){
		initQuery(historicProcess, new Page());
		List<ActCnHistoricProcess> list = handleService.selectUnfinishedProcessList(query);
		return initTableInfo(list, query);
	}
	
	/**
	 * 已办任务
	 * @return
	 */
	@RequiresPermissions("flowable:task:view")
	@GetMapping("/finished")
	public String finished(){
		return prefix + "/finished";
	}
	
	/**
	 * 任务数据
	 * @param historicProcess
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequiresPermissions("flowable:task:list")
	@PostMapping("/finishedList")
	@ResponseBody
	public TableInfo finishedList(ActCnHistoricProcess historicProcess){
		initQuery(historicProcess, new Page());
		List<ActCnHistoricProcess> list = handleService.selectFinishedProcessList(query);
		return initTableInfo(list, query);
	}
	
	/**
	 * 所有办结任务(一般是给管理员使用)
	 * @return
	 */
	@RequiresPermissions("flowable:task:view")
	@GetMapping("/complete")
	public String complete(){
		return prefix + "/complete";
	}
	
	/**
	 * 任务数据
	 * @param historicProcess
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequiresPermissions("flowable:task:list")
	@PostMapping("/completeList")
	@ResponseBody
	public TableInfo completeList(ActCnHistoricProcess historicProcess){
		initQuery(historicProcess, new Page());
		List<ActCnHistoricProcess> list = handleService.selectCompleteProcessList(query);
		return initTableInfo(list, query);
	}
	
	
	/**
	 * 任务办理页面（审批意见）
	 * @param businessKey 业务ID
	 * @param modelMap
	 * @return
	 */
	@GetMapping("/submit/{businessKey}")
	public String submit(@PathVariable("businessKey") String businessKey, ModelMap modelMap){
		List<Map<String, String>> handles = FlowableUtils.getNextHandle(businessKey);
		modelMap.put("businessKey", businessKey);
		modelMap.put("handles", handles);
		FlowElement currentNode = FlowableUtils.getCurrentFlowNode(businessKey);
		modelMap.put("currentNode", currentNode);
		return prefix + "/submit";
	}
	
	/**
	 * 保存结束任务办理
	 * @param businessKey 业务ID
	 * @param handleId 操作ID
	 * @param message 办理意见
	 * @return
	 */
	@RequiresPermissions("flowable:task:edit")
	@Log(title = "任务管理", businessType = BusinessType.UPDATE)
	@PostMapping("/submit")
	@ResponseBody
	public JsonResult submitSave(String businessKey, String handleId, String message, String userId,
			String attachmentName, String attachmentDescription, MultipartFile attachmentFile){
		
		handleService.generalDirectHandle(businessKey, handleId, message, userId, 
				attachmentName, attachmentDescription, attachmentFile);
		
		return JsonResult.success();
	}
	
	/**
	 * 获取选择下一步环节以及操作人员
	 * @param businessKey 业务ID
	 * @param modelMap
	 * @return
	 */
	@PostMapping("/assignee/{businessKey}/{handleId}")
	@ResponseBody
	public JsonResult assignee(@PathVariable("businessKey") String businessKey, @PathVariable("handleId") String handleId){
		List<SysUser> users = FlowableUtils.getNextNodeHandleUser(businessKey, handleId);
		FlowElement nextNode = FlowableUtils.getNextFlowNode(businessKey, handleId);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("users", users);
		result.put("nextNode", nextNode);
		return JsonResult.success(result);
	}
	
	/**
	 * 查看流程图
	 * @param modelId
	 * @param modelMap
	 * @return
	 */
	@RequiresPermissions("flowable:task:image")
	@GetMapping("/image/{businessKey}")
	public String image(@PathVariable("businessKey") String businessKey, ModelMap modelMap){
		modelMap.put("businessKey", businessKey);
		return prefix + "/image";
	}
	
	/**
	 * 生成流程图
	 * @param modelId
	 * @param response
	 */
	@GetMapping("/diagram/{businessKey}")
	public void diagram(@PathVariable("businessKey") String businessKey, HttpServletResponse response){
		try {
			InputStream in = diagramService.getFlowableDiagramByBusinessKey(businessKey);
			FileUtils.writeBytes(in, response.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 查看流程步骤详情
	 * @param modelId
	 * @param modelMap
	 * @return
	 */
	@RequiresPermissions("flowable:task:detail")
	@GetMapping("/detail/{businessKey}")
	public String detail(@PathVariable("businessKey") String businessKey, ModelMap modelMap){
		List<ActCnHistoricActivity> activitys = handleService.selectHistoricActivityByBusinessKey(businessKey);
		modelMap.put("activitys", activitys);
		return prefix + "/detail";
	}
	
}
